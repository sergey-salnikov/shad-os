#pragma once

static inline void atomic_add(long* atomic, long value)
{
	// Your code here.
}

static inline void atomic_sub(long* atomic, long value)
{
	// Your code here.
}

static inline long atomic_xchg(long *atomic, long value)
{
	// Your code here.
}

static inline int atomic_cmpxchg(long *atomic, long *expected, long value)
{
	// Your code here.
}


