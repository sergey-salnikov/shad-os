#pragma once

// Your code here.

struct rwlock {
    // Your code here.
};

static inline void rwlock_init(struct rwlock *s)
{
    // Your code here.
}

static inline void rwlock_read_lock(struct rwlock *s)
{
    // Your code here.
}

static inline void rwlock_read_unlock(struct rwlock *s)
{
    // Your code here.
}

static inline void rwlock_write_lock(struct rwlock *s)
{
    // Your code here.
}

static inline void rwlock_write_unlock(struct rwlock *s)
{
    // Your code here.
}
