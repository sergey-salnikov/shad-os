#include "syscall.h"

int main()
{
    int pipefd[2];
    pipe(pipefd);

    pid_t p = fork();
    if (!p) {
        char* argv[] = {"/bin/echo", "hi", 0};
        char* envp[] = {0};
        close(pipefd[0]);
        close(1);
        dup(pipefd[1]);
        execve("/bin/echo", argv, envp);
        exit(1);
    }
    close(pipefd[1]);

    char buf[1000];
    ssize_t r = read(pipefd[0], buf, sizeof(buf));
    if (r != 3 || buf[0] != 'h' || buf[1] != 'i' || buf[2] != '\n')
        return 1;

    int status;
    pid_t s = waitpid(p, &status, 0);
    if (s != p || (status & 0x7f) != 0)
        return 1;
    return 0;
}
