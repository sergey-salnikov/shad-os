#include <stdio.h>
#include <pthread.h>
#include <assert.h>
#include "atomic.h"

#define NITER (1<<16)
#define NTHREADS 50

struct spinlock s;
volatile long c;
volatile long d;

void* summer(void *arg)
{
    int i;
    for (i = 0; i < NITER; i++) {
        spinlock_lock(&s);
        assert(c == d);
        c++;
        d++;
        spinlock_unlock(&s);
    }
    return 0;
}

void run_threads(void* (*f) (void*), void *arg)
{
    int i;
    pthread_t tid[NTHREADS];
    for (i = 0; i < NTHREADS; i++){
        pthread_create(&tid[i], NULL, f, arg);
    }
    for (i = 0; i < NTHREADS; i++){
        pthread_join(tid[i], NULL);
    }
}

int main()
{
    spinlock_init(&s);
    run_threads(summer, NULL);
    assert(c == (long) NITER * NTHREADS);
    printf("Passed summer test\n");

    return 0;
}
