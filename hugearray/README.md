# hugearray 

В этой задаче мы реализуем массив, размер которого превышает размер доступной памяти.
Для этого придется воспользоваться техникой ленивого выделения и освобождения.

А именно, адрес массива указывает в область памяти, которая ничем не занята.
При обращении в массив ядро видит, что адрес невалидный, и доставляет сигнал `SIGSEGV` процессу.
В обработчике сигнала можно узнать адрес, из-за которого произошла ошибка: он доступен как элемент `si_addr` структуры `siginfo_t`.

Если обработчик видит. что адрес лежит в массиве, то он выделяет соответствующую страницу и наполняет ее значениями.
Какие значения должны быть в массиве можно понять по функции `calculate_sqrts`.

При этом, если все время выделять память, то она закончится, по-этому ранее выделенные страницы нужно освобождать, чтобы дать обратиться к новым элементам массива.

После того, как нужная страница заполнена значениями, можно просто вернуться из обработчика: после возвращения в обычное исполнение, инструкция, которая привела к `SIGSEGV` будет перевыполнена.

Вам нужно написать только сам код обработчика сигнала в файле `handler.c`.
 
