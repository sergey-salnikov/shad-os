#include "plugin.h"

#define TO_HEX(c) ((c) < 10 ? '0' + (c) : 'a' + (c) - 10)
#define FROM_HEX(c) ((c) <= '9' ? (c) - '0' : (c) - 'a' + 10)
#define IS_HEX(c) ((c) >= '0' && (c) <= '9' || (c) >= 'a' && (c) <= 'f')

void uefa_plugin_encode(struct stream *stream)
{
    char input[1024], output[2 * sizeof(input)];
    size_t size;
    while ((size = stream_read(stream, input, sizeof(input))) > 0) {
        for (int i = 0; i < size; ++i) {
            output[2*i] = TO_HEX(input[i] % 16);
            output[2*i + 1] = TO_HEX(input[i] / 16);
        }
        stream_write(stream, output, 2 * size);
    }
}

int uefa_plugin_decode(struct stream *stream)
{
    char input[2048], output[sizeof(input) / 2];
    size_t size;
    while ((size = stream_read(stream, input, sizeof(input))) > 0) {
        if (size % 2) {
            return 1;
        }
        for (int i = 0; i < size; i += 2) {
            char lo = input[i];
            char hi = input[i+1];
            if (!IS_HEX(lo) || !IS_HEX(hi)) {
                return 2;
            }
            output[i/2] = FROM_HEX(hi) * 16 + FROM_HEX(lo);
        }
        stream_write(stream, output, size / 2);
    }
    return 0;
}
