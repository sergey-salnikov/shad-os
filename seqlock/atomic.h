#pragma once

// Your code here.

struct seqlock {
    // Your code here.
};

static inline void seqlock_init(struct seqlock *s)
{
    // Your code here.
}

static inline long seqlock_read_lock(struct seqlock *s)
{
    // Your code here.
}

static inline int seqlock_read_unlock(struct seqlock *s, long v)
{
    // Your code here.
}

static inline void seqlock_write_lock(struct seqlock *s)
{
    // Your code here.
}

static inline void seqlock_write_unlock(struct seqlock *s)
{
    // Your code here.
}

